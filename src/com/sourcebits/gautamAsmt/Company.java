package com.sourcebits.gautamAsmt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.util.Log;

/**
 * @author cynogn
 * 
 */
public class Company implements Comparator<Company> {
	public static int count = 0;
	private String location;
	private String name;
	private String address;
	HashMap<Integer, Company> hashMap = new HashMap<Integer, Company>();
	private static final String TAG = "StartSceen";//Tag

	/**
	 * 
	 * @param Gets
	 *            the Company Name
	 */
	private void getName(String name1) {
		name = name1;
	}

	/**
	 * 
	 * @param Sets
	 *            the Company Name
	 */
	private String setName() {
		return name;
	}

	/**
	 * 
	 * @param Gets
	 *            the Company Location
	 */
	private void getLocation(String name1) {
		location = name1;
	}

	/**
	 * 
	 * @param Sets
	 *            the Company Location
	 */
	private String setLocation() {
		return location;
	}

	/**
	 * 
	 * @param Gets
	 *            the Company Address
	 */
	private void getAddress(String name1) {
		address = name1;
	}

	/**
	 * 
	 * @param Sets
	 *            the Company Address
	 */
	private String setAddress() {
		return address;
	}

	/**
	 * 
	 * @param inserts
	 *            values into Hash Map
	 * 
	 */
	public void insert(String valu1, String valu2, String valu3) {
		Company c1 = new Company();
		c1.getName(valu1);
		c1.getLocation(valu2);
		c1.getAddress(valu3);
		hashMap.put(count++, c1);
		Log.d(TAG, "SAVED . " + count + "Elements in Hash");
	}

	/**
	 * Sorting in Descending Order
	 */
	public void sortDescnding() {

		Collection<Company> stud = hashMap.values();
		ArrayList list = new ArrayList(stud);
		Collections.sort(list, new Company());
		int tmp = 1;
		Collections.reverse(list);
		Iterator it = list.iterator();
		while (it.hasNext()) {
			tmp++;
			Company comp = (Company) it.next();
			Log.d(TAG, "DESC NAME : " + comp.setName() + " \t"
					+ "LOCATION : " + comp.setLocation() + " \t" + "ADDRESS : "
					+ comp.setAddress());
		}

	}

	/**
	 * Sorting in Ascending Order
	 */
	public void sortAscending() {
		Collection<Company> stud = hashMap.values();
		List list = new ArrayList(stud);
		Collections.sort(list, new Company());
		int tmp = 1;
		Iterator it = list.iterator();
		while (it.hasNext()) {
			tmp++;
			Company comp = (Company) it.next();
			Log.d(TAG, "ASC NAME : " + comp.setName() + " \t"
					+ "LOCATION : " + comp.setLocation() + " \t" + "ADDRESS : "
					+ comp.setAddress());
		}
	}

	/*
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 * comparing company name
	 */
	@Override
	public int compare(Company lhs, Company rhs) {
		return lhs.name.compareTo(rhs.name);
	}
}

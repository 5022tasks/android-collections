package com.sourcebits.gautamAsmt;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * StartScreen Activity to Save the company details
 * @author Gautam
 */
public class StartScreen extends Activity implements OnClickListener {
	
	private Company comp = new Company();//Object of Compant type define in Company.java
	EditText mNameEditText;  //Company Name
	EditText mLocationEditText;//CompanyLocation
	EditText mAddressEditText;//Company Address
	Button mSaveButton;//Save Button
	Button mClearButton;//Clear Button
	Button mAscButton;//Ascending Button
	Button mDesButton;//Descending Button
	private static final String TAG = "StartSceen";//Tag

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_screen);

		mNameEditText = (EditText) findViewById(R.id.compNameEditText);

		mLocationEditText = (EditText) findViewById(R.id.EditText01);

		mAddressEditText = (EditText) findViewById(R.id.EditText02);

		mSaveButton = (Button) findViewById(R.id.button1);

		mClearButton = (Button) findViewById(R.id.button2);

		mAscButton = (Button) findViewById(R.id.Button01);

		mDesButton = (Button) findViewById(R.id.Button02);

		/* Setting Action Listener for all buttons */
		mSaveButton.setOnClickListener(this);
		mClearButton.setOnClickListener(this);
		mAscButton.setOnClickListener(this);
		mDesButton.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.button1: // Save Button action
			Log.d(TAG, "SAVING...");
			if (mNameEditText.getText().toString().equals("")) {
				Log.d(TAG, "NOT SAVED! Name Field is Empty");
			} else if (mLocationEditText.getText().toString().equals("")) {
				Log.d(TAG, "NOT SAVED! Location Field is Empty");
			} else if (mAddressEditText.getText().toString().equals("")) {
				Log.d(TAG, "NOT SAVED! Address Field is Empty");
			} else
				comp.insert(mNameEditText.getText().toString(),
						mLocationEditText.getText().toString(),
						mAddressEditText.getText().toString());
			break;

		case R.id.button2: // Clear Button action
			Log.d(TAG, "Cleared!");
			mNameEditText.setText("");
			mLocationEditText.setText("");
			mAddressEditText.setText("");
			break;

		case R.id.Button01: // Ascending Button action
			comp.sortAscending();
			break;
		case R.id.Button02: // Descending Button action
			comp.sortDescnding();
			break;
		default:
			break;
		}

	}

}
